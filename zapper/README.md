# Zapper
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

# Prerequisites
Install nvm if it is not installed (https://github.com/creationix/nvm#installation)

P.S:ensure that terminal directory is frontend/zapper
#----------------------------------
Install [Node.js] version `v7.7.2`-
#----------------------------------
$ nvm install v7.7.2
$ nvm use v7.7.2
#----------------------------------
install [@angular/cli] 
#----------------------------------
$ npm install -g @angular/cli@1.0.0-rc.2
$ npm install
# For Build 
$ ng build
# For Running
$ npm start

copy this url "http://localhost:4200/ " in your browser,
